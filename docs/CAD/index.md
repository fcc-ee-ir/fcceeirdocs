# Interaction Region CAD Files

The cad files are available in the GitLab repository [https://gitlab.cern.ch/fcc-ee-ir/cad](https://gitlab.cern.ch/fcc-ee-ir/cad)


The files are organized as follows:

- **FCC-ee IF**: 

- **FCC-ee IR**:


To create a local copy of the files to a directory use:
```
git clone https://gitlab.cern.ch/fcc-ee-ir/cad.git
```
