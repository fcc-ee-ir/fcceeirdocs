# FCC-ee Interaction Region Docs Web

In this site we will collect documentation, tools and other information related to FCC-ee Interaction Region design and studies.

The goal is to share information, keep track of updates and provide tools and hints for the studies. 

It is organized as follows:

- **CAD** : CAD files for the IR region 
- **Software** : software packages and tools
- **How-tos** : 

